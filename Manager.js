const mongoose = require('mongoose')
const taskSchema = require('./schemas/taskSchema')
const Task = mongoose.model('task', taskSchema)
const ExcelToJsonTable = require('./manager/ExcelToJsonTable')
const JsonTableToOracle = require('./manager/JsonTableToOracle')
const axios = require('axios')
const Settings = require('./Settings')
const ApiUrl = `${Settings.BaseUrl}:${Settings.ApiPort}` || 'localhost:5000'

mongoose.connect(Settings.MongoDb, {
  useNewUrlParser: true,
  useFindAndModify: false,
})
.catch(err => {
  console.error(err)
})

let db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))

function main(){
  Task.findOne({processed: false, running: false, error: undefined}).sort({_id: 1})
    .then(doc => {
      if (!doc){
        let twoDaysOld = new Date()
        twoDaysOld.setDate(twoDaysOld.getDate() - 2)
        Task.findOneAndDelete({createdDate: { $lt: twoDaysOld, processed: true, error: undefined}})
        return
      }

      axios.post(`${ApiUrl}/api/queue/started`, {id: doc._id})
        .then(() => ExcelToJsonTable(doc.filePath))
        .then(jsonTable => {
          if (doc.dbType === 'oracle'){
            return JsonTableToOracle(doc.tableName, jsonTable)
          }else if (doc.dbType === 'mssql'){
            // TODO create JsonTableToMSSQL
            return new Promise((resolve, reject) => reject('Not implemented yet.'))
          }
        })
        .then(result => {
          if (!result.success){
            return console.error(result.err)
          } 

          console.log(`  Inserted ${result.rows} rows into ${doc.tableName}.`)
          axios.post(`${ApiUrl}/api/queue/completed`, {id: doc._id})
        })
        .catch(error => {
          console.error(error)
          axios.post(`${ApiUrl}/api/queue/error`, {id: doc._id, error})
        })
    })
    .catch(err => console.log('Find error: ', error))
}

setInterval(main, 2000)