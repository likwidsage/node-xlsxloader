require('dotenv').config()

const Settings = {
  uploadDir: process.env.UPLOAD_DIR,
  BaseUrl: process.env.API_URL,
  ApiPort: process.env.API_PORT,
  MongoDb: process.env.MONGO_URL,
  Oracle: {
    user: process.env.ORACLE_USER,
    password: process.env.ORACLE_PASS,
    connectString: process.env.ORACLE_SERVER,
  },
}

module.exports = Settings
