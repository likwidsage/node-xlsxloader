const fs = require('fs')
const XLSX = require('xlsx')

/** Create a JSON table where every field is always represented from an Excel file
 * @param {string} file Name of Table to create
 */
function ExcelToJsonTable(file){
  return new Promise((resolve, reject) => {

    if (!fs.existsSync(file)){
      reject(`Can't find locate ${file}`)
    }

    // Convert Excel to JSON
    console.log(`Opening ${file}...`)
    const workbook = XLSX.readFile(file)
    const sheet = workbook.Sheets[workbook.SheetNames[0]]
    const json = XLSX.utils.sheet_to_json(sheet, {defval: null})

    // change json to make it importable
    const returnArray = json.map(c => {
      let newObject = { ...c }
      delete newObject.__rowNum__
      for (let name in newObject){
        let newName = name
          .trim()
          .replace(/[\s]+/g, '_')
          .replace(/[^a-zA-Z0-9\s]+/g, '')
          .substring(0,30)

        if(newName !== name){
          for (let number = 1; newObject.hasOwnProperty(newName); number++) {
            newName = newName
              .trim()
              .substring(0,30-number.toString().length) + number
          }

          switch (typeof newObject[name]) {
          case 'string':
            newObject[newName] = newObject[name].trim()
            break
          case 'number':
            newObject[newName] = newObject[name].toString().trim()
            break
          default: 
            newObject[newName] = newObject[name]
          }
          delete newObject[name]
        }
      }
      return newObject
    })
    resolve(returnArray)
  })
}

module.exports = ExcelToJsonTable