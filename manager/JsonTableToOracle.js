const oracledb = require('oracledb')
const {Oracle} = require('../Settings')

/** Create a table from a JSON Structure where JSON structure has all fields in every enum
 * @param {string} tableName Name of Table to create
 * @param {JSON} jsonTable JSON Structure where JSON structure has all fields in every enum
 */
function JsonTableToOracle(tableName, jsonTable){
  console.log('Connecting to Oracle...')
  const columnNames = Object.keys(jsonTable[0])

  return new Promise((resolve, reject) => {
    oracledb.getConnection(Oracle)
      .then(conn => {
      // Remove existing table if exists
        console.log(`Dropping Table ${tableName}`)
        return conn.execute(`Drop Table ${tableName}`)
          .then(() => {
            console.log('  Table dropped.')
            return conn
          })
          .catch(err => {
            console.error('Drop:', err)
            return conn
          })
      })
      .then(conn => {
      // Create table
        let query = `Create Table ${tableName} (\r`
        columnNames.forEach((name, ind) => {
          query += `  ${name} varchar(500)${ind !== columnNames.length - 1 ? ',' : ''}`
          query+='\r'
        })
        query += ')'
    
        console.log(`Creating table ${tableName}`)
        return conn.execute(query)
          .then(() => {
            console.log(`  Table ${tableName} created.`)
            return conn
          })
          .catch(err => {
            reject({err, query})
          })
      })
      .then(conn => {
        //Generate Bind Query
        let bindQuery = `INSERT INTO ${tableName} (`
        columnNames.forEach((name, i) => bindQuery += `${name}${i !== columnNames.length - 1 ? ',' : ''}` )
        bindQuery += ') VALUES ('
        columnNames.forEach((name, i) => bindQuery += `:${name}${i !== columnNames.length - 1 ? ',': ''}` )
        bindQuery += ')'

        //Insert using bind
        console.log('Inserting values')
        conn.executeMany(bindQuery, jsonTable, {autoCommit: true})
          .then(res => {
            resolve({success: true, rows: res.rowsAffected})
          })
          .catch(err => {
            reject({success: false, err: err})
          })
      })
  })
}

module.exports = JsonTableToOracle