const mongoose = require('mongoose')

const runnerSchema = new mongoose.Schema(
  {
    taskId: String,
    startTime: Date,
    percentage: Number,
    endTime: Date,
    complete: {
      type: Boolean,
      default: false,
    },
  }
)

module.exports = runnerSchema
